# TypeScript
TypeScript is a superset of JavaScript that will add an optional static typing to JavaScript projects. At first static types looks like they are enforcing something rising warnings in places you did not have them previously. But in a long it will prevent you from doing some of the most common errors. Most of IDE will leverage static types providing you with intellisense for strongly typed code. This will speed up the development process and demand less documentation for your code. Simply speaking if you use some object in your code you will know all of its members without looking directly to its code. 

## Adding TS package
Adding Type Script to already created react-app is very simple. You just need to install type script packages. Npm will help us with this.

```
npm install --save typescript @types/node @types/react @types/react-dom
```

It will install Type Script and types definitions (@type) for react libraries.

## Converting code
No we can convert JS code to TS. To do that we need to change extensions of our files from *.js to *.ts and *jsx to *.tsx. In some cases we need to add missing libraries and types afterwards.

## Creating TS app from scratch
There is even a simpler way to create React with Type Script app. We can use the same script as in previous chapter to do that with a --typescript option

```
npm init react-app app-name --typescript
```