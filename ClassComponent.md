# Class component
This is the most common approach to define React components. It is more verbose, but has a longer syntax then functional approach, that will be presented in the next chapter. Class components syntax define React life cycle methods in more explicit way by overiding funcitions like *componentDidMount* 

## Component definition
To define a class component we need to define a class and extend React definition of a basic Component.

```
export default class App extends React.Component {
  ...
}
```

The next important thing is to override render function and return component html definition. This is done by returning a JSX that extend basic HTML.

```
export default class App extends React.Component {
  render() {
    return (
      <div>...</div>
    )
  }
}
```

Component does not have to be only a simple div, but also include other html tags. It is up to you what component will do and present. Remember that it is good design if a component has a single responsibility.

## Properties
All components can have properties. This way parent components can manage their sub components. When using Type Script we can define properties through an interface. It is as simple as exporting proper definition

```
export interface AppProps {
  onClick: () => void;
}
```

and apply it when defining component

```
export default class App extends React.Component<AppProps, {}> {
  ...
}
```

In our above example, we are passing a callback function to our component. We will use it as our *onClick* handler. 

```
render() {
  return (
    <img onClick={this.props.onClick.bind(this)} src={logo} className="App-logo" alt="logo" />
  );
}
```

All the props of a component can be accessed through *this.props* object. In our example we used bind() function. This is because our callback will be called asynchronously when user will click the image and we need to tell Java Script  which object we are referring as *this*. 


## State
Apart from properties components can have also an internal state. Same as with properties we need to define state interface and pass in component definition.

```
export interface AppState {
  backgroundColor: string;
}

export default class App extends React.Component<AppProps, AppState> {
  ...
}
```

As state is internal part of a component it have to be set when component is created. We can do this inside of our component definition.


```
export default class App extends React.Component<AppProps, AppState> {
  state = {
    backgroundColor: "#282c34" 
  }
  ...
}
```

Now we can use state values in render function the same as we used our props.

```
render() {
  return (
    <div className="App" style={{backgroundColor: this.state.backgroundColor}} 
      <img onClick={this.props.onClick.bind(this)} src={logo} className="App-logo" alt="logo" />
    </div>
  );
}
```

This way we set initial background color when component is displayed. 

State is not constant and it can change. We just need to tell React that we assigned new value to it. This is done by running *this.setState* method with state changes. We will now randomly pick color of our application background when 
*randomBackgroud* function is run.

```
randomBackground() {
  const randomColor = Math.floor(Math.random()*16777215).toString(16);
  this.setState({
    backgroundColor: `#${randomColor}`
  })
}
```

This will set new state of *this.state.backgroundColor*. We just need to assign *randomBackground* function to for example *onClick* event.

```
render() {
  return (
    <div className="App" style={{backgroundColor: this.state.backgroundColor}} onClick={this.randomBackground.bind(this)}>
        <img onClick={this.props.onClick.bind(this)} src={logo} className="App-logo" alt="logo" />
    </div>
  );
}
```

Again we need to remember to bind proper *"this"* to our function. Now on component click, background color will change.

## Lifecycle methods

We will use only one from various lifecycle methods that can be used to demonstrate how they can be used. We picked the simplest on *componentDidMount*. This method will be called by React runtime when component will be mounted (displayed) by the application. We will use simple alert to indicate that everything works.

```
componentDidMount() {
    alert("Component mounted");
  }
```

