import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';

export interface AppProps {
  onClick: () => void;
}

export interface AppState {
  backgroundColor: string;
}

export const App: React.FunctionComponent<AppProps> = ({onClick}) => {
  const [backgroundColor, setBackgroundColor] = useState("#282c34");

  useEffect(() => {
    alert("Component mounted");
  }, []);

    const randomBackground = () => {
    const randomColor = Math.floor(Math.random()*16777215).toString(16);
    setBackgroundColor(`#${randomColor}`);
  }

  
  return (
    <div className="App" style={{backgroundColor}} onClick={randomBackground}>
      <header className="App-header">
        <img onClick={onClick} src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
};