import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { App } from './App';

const onClick = () => {
  alert('Do not click the image!');
}

ReactDOM.render(
  <React.StrictMode>
    <App onClick={onClick}/>
  </React.StrictMode>,
  document.getElementById('root')
);
