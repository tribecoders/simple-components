import React from 'react';
import { render } from '@testing-library/react';
import { App } from './App';
import '@testing-library/jest-dom/extend-expect';



test('renders learn react link', () => {
  const { getByText } = render(<App onClick={()=>{}}/>);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

test('call calback on image click', () => {
  const onClickMock = jest.fn(() => {})
  const application= render(<App onClick={onClickMock}/>);
  const imgElement = application.getByRole('img')
  imgElement.click();
  expect(onClickMock.mock.calls.length).toBe(1);
});
