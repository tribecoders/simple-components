# Initial project

This Simple React project was created using Create React App. To learn more go to [https://github.com/facebook/create-react-app](https://github.com/facebook/create-react-app).
Follow below steps to create your own simple project.

## Perquisites installation
If you have not installed Node.js on your computer. Head to [https://nodejs.org/](https://nodejs.org/) and follow instructions there. This will install node with npm JavaScript package manager which will be needed for further tasks. 


## Generate new project
To initialise a project you need to run one simple command

```
npm init react-app app-name
```

This will initialise an app in new directory app-name. And install all required node_modules (npm packages)

Now you just need to go to application directory and run it.

```
cd app-name
npm run start
```

Build in development web server will start and application should be displayed in your browser.

See the process [here](https://www.instagram.com/p/B9485MRnaC9/?utm_source=ig_web_button_share_sheet)



