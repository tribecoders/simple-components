# Function Component
This is more modern approach to define components. It has couple of benefits. Starting from function components is less code and much more readable structure. The other benefit is access to React hooks which can extend your components capabilities.

## Component definition
Similar to class component we can define properties interface to assign to components. Below you can find a sample functional component definition.

```
export const App: React.FunctionComponent<AppProps> = ({onClick}) => {
  ...
}
```

Lets read what happened here. We define constant  App (our component) as React Function Component with properties interface AppProps. We define this constant  as arrow function *() => {}* taking as parameter defined earlier properties. We split properties object to single variables. In our case *onClick* function.

State is a little bit different. We are not define on state object with several fields. We can now define separate variables and their setters using *useState* hook.

```
const [backgroundColor, setBackgroundColor] = useState("#282c34");
```

This will create *backgroundColor* variable and a setter *setBackgroundColor* function. When *setBackgroundColor* will be invoked *backgroundColor* will be set and React will know that component need to re-render. When defining state variables we can define their initial value at the same time. In our case we set the default color value.

We can now update *randomBackground* function. It will be simpler as we do not have to set the whole state just *backgroundColor*

```
const randomBackground = () => {
  const randomColor = Math.floor(Math.random()*16777215).toString(16);
  setBackgroundColor(`#${randomColor}`);

}
```

## Render
Functional components do not have render function. They just need to return JSX code.

```
return (
  <div className="App" style={{backgroundColor}} onClick={randomBackground}>
    <img onClick={onClick} src={logo} className="App-logo" alt="logo" />
  </div>
);
```

As you can see both callback functions and variables definitions are much more simple. We do not have to bind *"this"* to functions as arrow definitions of them will keep proper *"this"*.


## Life cycle methods
Life cycle methods do not exist in functional component but we can still define *useEffect** hook that can act as some of them.

```
useEffect(() => {
  alert("Component mounted");
}, []);
```

This defined function is called whenever variables in second parameter array change. That is why our array is empty. This will run our function only once when component is displayed acting in similar way as *componentDidMount*
